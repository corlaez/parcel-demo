import React from "react";
import { render } from "react-dom";
import store from "./reduxstore/Store";
import { Provider } from "react-redux";
import App from "./App";

if (window.ctx == null) {
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  window.ctx = new window.AudioContext();
}

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

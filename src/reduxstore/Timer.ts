import initialState from "./initialState";

class TimerState {
  public static INCREMENT = "Timer/INCREMENT";

  static count(by = 1) {
    return {
      type: TimerState.INCREMENT,
      payload: by
    };
  }

  static reducer(state = initialState.time, action) {
    switch (action.type) {
      case TimerState.INCREMENT:
        return state + action.payload;
      default:
        return state;
    }
  }
}

export default TimerState;

import React from "react";
import Time from "./Time";
import Interval from "./Interval";

class Timer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      auto: props.auto != null ? props.auto : false,
      time: props.time != null ? props.time : 0
    };
  }

  flip = () => {
    this.setState(prev => ({
      auto: !prev.auto
    }));
  };

  count = (by = 1) => {
    if (this.props.count) this.props.count(by);
    else
      this.setState(prev => ({
        time: prev.time + by
      }));
  };

  time = () => {
    if (this.props.time != null) return this.props.time;
    else return this.state.time;
  };

  render() {
    return (
      <div
        style={{
          textAlign: "center",
          fontSize: 50,
          border: 5,
          borderStyle: "solid",
          borderRadius: 50
        }}
      >
        <Time value={this.time()} />
        <button onClick={() => this.count(-this.time())}>reset</button>
        <button onClick={() => this.count()}>+</button>
        <button onClick={() => this.count(-1)}>-</button>
        <button onClick={this.flip}>{this.state.auto ? "stop" : "play"}</button>
        <Interval callback={() => this.count()} enabled={this.state.auto} />
      </div>
    );
  }
}

export default Timer;

import React from "react";
import Mouse from "../Mouse";
import DeepPure from "../common/deep-pure";
import Pure from "../common/pure";
import Oscillator from "./Oscillator";

import Time from "../Time";
import Timer from "../Timer";
import TimerRedux from "../TimerRedux";

class Theremin extends React.Component {
  state = { height: 0, width: 0 };

  componentDidMount() {
    const height = this.wrapper.clientHeight;
    const width = this.wrapper.clientWidth;
    this.setState({ height, width });
  }

  render() {
    const { height, width } = this.state;
    return (
      <Mouse
        render={mouse => (
          <div
            ref={input => {
              this.wrapper = input;
            }}
          >
            <Pure>{this.props.children}</Pure>
            <Oscillator pitch={height - mouse.y} volume={1} />
          </div>
        )}
      />
    );
  }
}

export default Theremin;

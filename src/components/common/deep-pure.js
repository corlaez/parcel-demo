import React from "react";
import DeepPureComponent from "./deep-pure-component";

React.DeepPureComponent = DeepPureComponent;

class DeepPure extends React.DeepPureComponent {
  render() {
    return this.props.children == undefined ? null : this.props.children;
  }
}

export default DeepPure;

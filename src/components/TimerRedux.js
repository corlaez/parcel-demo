import React from "react";
import { connect } from "react-redux";
import TimerState from "../reduxstore/Timer";
import Timer from "./Timer";

class TimerRedux extends React.Component {
  render() {
    const props = this.props;
    return <Timer time={props.time} count={props.count} />;
  }
}

export default connect(mapS, mapD)(TimerRedux);

function mapS(state) {
  return {
    time: state.time
  };
}

function mapD(dispatch) {
  return {
    count: by => dispatch(TimerState.count(by))
  };
}

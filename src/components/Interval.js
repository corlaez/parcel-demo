import React from "react";

export default class ReactInterval extends React.PureComponent {
  static defaultProps = {
    enabled: true,
    timeout: 1000
  };

  componentDidMount() {
    this.sideEffects();
  }

  componentDidUpdate() {
    this.sideEffects();
  }

  componentWillUnmount() {
    this.stop();
  }

  sideEffects = () => {
    if (this.props.enabled) {
      this.start();
    } else {
      this.stop();
    }
  };

  start = () => {
    this.stop();
    this.timer = setTimeout(this.props.callback, this.props.timeout);
  };

  stop = () => {
    clearTimeout(this.timer);
  };

  render() {
    return null;
  }
}

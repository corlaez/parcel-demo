import React from "react";
import Time from "./components/Time";
import Timer from "./components/Timer";
import TimerRedux from "./components/TimerRedux";
import Theremin from "./components/Theremin/Theremin";
import Oscillator from "./components/Theremin/Oscillator";

export default class App extends React.Component {
  render() {
    return (
      <Theremin>
        Time: Sencillo interpretador de segundos. (LOC: 16)
        <Time value={121} />
        Timer: Un Timer con controles para resetear, modificar y auto aumentar
        (LOC: 55)
        <br />
        <Timer />
        <br />
        <br />
        TimerRedux: Un Timer como el anterior pero el estado está contenido
        fuera del componente (controlado) *si abres la consola verás la
        impresión por cambio en redux* (LOC: 22)
        <br />
        <br />
        <TimerRedux />
        {/*TimerRedux is exposed with connect() so it is Pure*/}
        <br />
        <br />
        El primer componente es usado por el segundo para mostrar el tiempo El
        tercer componente usa el segundo componente y simplemente lo controla
        pasando propiedades Aunque el segundo y el tercero funcionan exactamente
        igual, el estado del segundo componente es dificil de consultar desde
        otras partes de la aplicacion.
        <br />
        El desarrollo por componentes permite reducir la complejidad y el reuso
        del codigo de forma natural. Así pues si mejoro el pequeño componente
        Time (de hecho antes no se renderizaban bien los negativos), los otros 2
        componenetes tendrán la mejora inmediatamente. (Por ejemplo el
        componente que engloba toda esta pagina solo 1 LOC para mostrar
        cualquiera de los componentes descritos )
        <br />
        El componente Timer ha sido escrito siguiendo patrones declarativos que
        permiten eliminar el tiempo de la aplicación.
        <br />
        <br />Armando Córdova.
        <br />
      </Theremin>
    );
  }
}
